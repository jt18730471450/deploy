#!/bin/sh

############# run images ############
oc run datafoundry-citic-web --image registry.dataos.io/datafoundry_citic/datafoundry-citic-web
oc run datafoundryrecharge --image registry.dataos.io/datafoundry_citic/datafoundryrecharge
oc run datafoundryserviceusage --image registry.dataos.io/datafoundry_citic/datafoundryserviceusage
oc run datafoundryoauth --image registry.dataos.io/datafoundry_citic/datafoundryoauth
oc run dataintegration --image registry.dataos.io/datafoundry_citic/dataintegration
oc run datafoundryservicevolume --image registry.dataos.io/datafoundry_citic/datafoundryservicevolume
oc run datafoundrydatainstance --image registry.dataos.io/datafoundry_citic/datafoundrydatainstance
oc run datafoundrypayment --image registry.dataos.io/datafoundry_citic/datafoundrypayment
oc run datafoundrycoupon --image registry.dataos.io/datafoundry_citic/datafoundrycoupon
oc run datafoundryplan --image registry.dataos.io/datafoundry_citic/datafoundryplan


############## expose svc ############


oc expose dc datafoundry-citic-web --port 80
oc expose dc datafoundryrecharge --port 8090
oc expose dc datafoundryserviceusage --port 3000
oc expose dc datafoundryoauth --port 9443
oc expose dc dataintegration --port 8092
oc expose dc datafoundryservicevolume --port 9095
oc expose dc datafoundrydatainstance --port 8574
oc expose dc datafoundrypayment --port 8080
oc expose dc datafoundrycoupon --port 8574
oc expose dc datafoundryplan --port 8574


############## set envs ##############


# deploymentconfigs datafoundry-citic-web, container datafoundry-citic-web
# API_PROXY_ADDR is HOST:PORT of svc 'datafoundrypayment'
# API_OAUTH_ADDR is HOST:PORT of svc 'datafoundryoauth'
# REDIS_PASSWORD is optional

oc env dc/datafoundry-citic-web REDIS_HOST=10.1.50.57 \
		REDIS_PORT=6379 \
		API_SERVER_ADDR=10.1.130.134:8443 \
		API_PROXY_ADDR=172.30.197.35:8080 \
		API_OAUTH_ADDR=172.30.214.116:9443 \
		API_METRICS_ADDR=localhost:12345 \
		ROUTER_DOMAIN_SUFFIX=.dataapp.c.citic


# deploymentconfigs datafoundrycoupon, container datafoundrycoupoon

oc env dc/datafoundrycoupon DATAFOUNDRY_ADMIN_PASS=RTYUdas \
		DATAFOUNDRY_ADMIN_USER=chaizs \
		DATAFOUNDRY_HOST_ADDR=10.1.130.134:8443 \
		DATAFOUNDRY_INFO_CN_NORTH_1=10.1.130.134:8443 adminuser passofadmin \
		DATAFOUNDRY_INFO_CN_NORTH_2=10.1.130.134:8443 adminuser passofadmin \
		ENV_NAME_DATAFOUNDRYRECHARGE_SERVICE_HOST=DATAFOUNDRYRECHARGE_SERVICE_HOST \
		ENV_NAME_DATAFOUNDRYRECHARGE_SERVICE_PORT=DATAFOUNDRYRECHARGE_SERVICE_PORT \
		ENV_NAME_MYSQL_ADDR=BSI_MYSQL_MYSQLFORCOUPON_HOST \
		ENV_NAME_MYSQL_DATABASE=BSI_MYSQL_MYSQLFORCOUPON_NAME \
		ENV_NAME_MYSQL_PASSWORD=BSI_MYSQL_MYSQLFORCOUPON_PASSWORD \
		ENV_NAME_MYSQL_PORT=BSI_MYSQL_MYSQLFORCOUPON_PORT \
		ENV_NAME_MYSQL_USER=BSI_MYSQL_MYSQLFORCOUPON_USERNAME \
		BSI_MYSQL_MYSQLFORCOUPON_HOST=mysqlnase.scebroker.dataos.io \
		BSI_MYSQL_MYSQLFORCOUPON_NAME=f580f931b \
		BSI_MYSQL_MYSQLFORCOUPON_PASSWORD=8097d8 \
		BSI_MYSQL_MYSQLFORCOUPON_PORT=3306 \
		BSI_MYSQL_MYSQLFORCOUPON_USERNAME=4f1107b5 \
		ADMINUSERS=admin datafoundry

# deploymentconfigs datafoundrydatainstance, container datafoundrydatainstance
											      
oc env dc/datafoundrydatainstance DATAFOUNDRY_HOST_ADDR=10.1.130.134:8443 \
		ENV_NAME_MYSQL_ADDR=BSI_MYSQL_MYSQLFORDATAINSTANCE_HOST \
		ENV_NAME_MYSQL_DATABASE=BSI_MYSQL_MYSQLFORDATAINSTANCE_NAME \
		ENV_NAME_MYSQL_PASSWORD=BSI_MYSQL_MYSQLFORDATAINSTANCE_PASSWORD \
		ENV_NAME_MYSQL_PORT=BSI_MYSQL_MYSQLFORDATAINSTANCE_PORT \
		ENV_NAME_MYSQL_USER=BSI_MYSQL_MYSQLFORDATAINSTANCE_USERNAME \
		BSI_MYSQL_MYSQLFORDATAINSTANCE_HOST=mysqlnoce.seker.dataos.io \
		BSI_MYSQL_MYSQLFORDATAINSTANCE_NAME=764bca316 \
		BSI_MYSQL_MYSQLFORDATAINSTANCE_PASSWORD=849fd7cb \
		BSI_MYSQL_MYSQLFORDATAINSTANCE_PORT=3306 \
		BSI_MYSQL_MYSQLFORDATAINSTANCE_USERNAME=2e15306

# deploymentconfigs datafoundryoauth, container datafoundryoauth
																
oc env dc/datafoundryoauth BSI_REDIS_REDISOAUTH_HOST=172.30.249.140 \
		BSI_REDIS_REDISOAUTH_NAME=cluster-sb-xxvpc6-redis \
		BSI_REDIS_REDISOAUTH_PASSWORD=ea67999a \
		BSI_REDIS_REDISOAUTH_PORT=26379 \
		BSI_REDIS_REDISOAUTH_URI= \
		BSI_REDIS_REDISOAUTH_USERNAME= \
		BSI_REDIS_REDISOAUTH_VHOST= \
		VCAP_SERVICES={"Redis":[{"name":"Redis_Oauth","label":"","plan":"standalone","credentials":{"Host":"172.30.249.140","Name":"cluster-sb-xhpc6-redis","Password":"ea6d1ba","Port":"26379","Uri":"","Username":"","Vhost":""}}]} \
		REDIS_SERVER_PARAMS=10.1.50.57+6379+ \
		DATAFOUNDRY_HOST_ADDR=https://10.1.130.134:8443 \
		ETCD_HTTP_ADDR=http://10.1.50.57 \
		ETCD_HTTP_PORT=2379 \
		ETCD_PASSWORD=6EA74-7D-4B-964C31A \
		ETCD_USER=asiaP \
		GITHUB_CLIENT_ID=23123432169e815b4 \
		GITHUB_CLIENT_SECRET=510bb1234323432290c64073a97f92710332c98 \
		GITHUB_REDIRECT_URL=http://datafoundryoauth-datafoundry.app-dacp.dataos.io/v1/repos/github-redirect


# deploymentconfigs datafoundrypayment, container datafoundrypayment
#ENV_***_HOST/PORT are svc info

oc env dc/datafoundrypayment ENV_MARKET_HOST=DATAFOUNDRYPLAN_SERVICE_HOST \
		ENV_MARKET_PORT=DATAFOUNDRYPLAN_SERVICE_PORT \
		ENV_CHECKOUT_HOST=DATAFOUNDRYSERVICEUSAGE_SERVICE_HOST \
		ENV_CHECKOUT_PORT=DATAFOUNDRYSERVICEUSAGE_SERVICE_PORT \
		ENV_BALANCE_HOST=DATAFOUNDRYRECHARGE_SERVICE_HOST \
		ENV_BALANCE_PORT=DATAFOUNDRYRECHARGE_SERVICE_PORT \
		ENV_COUPON_HOST=DATAFOUNDRYCOUPON_SERVICE_HOST \
		ENV_COUPON_PORT=DATAFOUNDRYCOUPON_SERVICE_PORT \
		ENV_DATAINSTANCE_HOST=DATAFOUNDRYDATAINSTANCE_SERVICE_HOST \
		ENV_DATAINSTANCE_PORT=DATAFOUNDRYDATAINSTANCE_SERVICE_PORT \
		APISERVER_CN_NORTH_01=10.1.130.134:8443 \
		APISERVER_CN_NORTH_02=10.1.130.134:8443 \
		DATAFOUNDRY_ADMIN_PASS=rrrrrrrrrrr \
		DATAFOUNDRY_ADMIN_USER=chaizs \
		ENV_INTEGRATION_HOST=DATAINTEGRATION_SERVICE_HOST \
		ENV_INTEGRATION_PORT=DATAINTEGRATION_SERVICE_PORT

# deploymentconfigs datafoundryplan, container datafoundryplan

oc env dc/datafoundryplan DATAFOUNDRY_HOST_ADDR=10.1.130.134:8443 \
		ENV_NAME_MYSQL_ADDR=BSI_MYSQL_MYSQLFORPLAN_HOST \
		ENV_NAME_MYSQL_DATABASE=BSI_MYSQL_MYSQLFORPLAN_NAME \
		ENV_NAME_MYSQL_PASSWORD=BSI_MYSQL_MYSQLFORPLAN_PASSWORD \
		ENV_NAME_MYSQL_PORT=BSI_MYSQL_MYSQLFORPLAN_PORT \
		ENV_NAME_MYSQL_USER=BSI_MYSQL_MYSQLFORPLAN_USERNAME \
		BSI_MYSQL_MYSQLFORPLAN_HOST=mysqlno.seicer.dataos.io \
		BSI_MYSQL_MYSQLFORPLAN_NAME=cd475ff8bf9 \
		BSI_MYSQL_MYSQLFORPLAN_PASSWORD=984b18e4 \
		BSI_MYSQL_MYSQLFORPLAN_PORT=3306 \
		BSI_MYSQL_MYSQLFORPLAN_USERNAME=cd407a29

# deploymentconfigs datafoundryrecharge, container datafoundryrecharge

oc env dc/datafoundryrecharge AIPAY_WEB_URL=https://121.31.32.100:8443/aipay_web/aiPay.do \
		BSI_MYSQL_RECHARGETEST_HOST=mysqlnocase.servicebroker.dataos.io \
		BSI_MYSQL_RECHARGETEST_NAME=be1b240d \
		BSI_MYSQL_RECHARGETEST_PASSWORD=68d017 \
		BSI_MYSQL_RECHARGETEST_PORT=3306 \
		BSI_MYSQL_RECHARGETEST_USERNAME=1949e6 \
		DataFoundryRegionOneHost=https://10.1.130.134:8443 \
		DataFoundryRegionTwoHost=https://10.1.130.134:8443 \
		ENV_NAME_MYSQL_ADDR=BSI_MYSQL_RECHARGETEST_HOST \
		ENV_NAME_MYSQL_DATABASE=BSI_MYSQL_RECHARGETEST_NAME \
		ENV_NAME_MYSQL_PASSWORD=BSI_MYSQL_RECHARGETEST_PASSWORD \
		ENV_NAME_MYSQL_PORT=BSI_MYSQL_RECHARGETEST_PORT \
		ENV_NAME_MYSQL_USER=BSI_MYSQL_RECHARGETEST_USERNAME \
		JAVA_AIPAY_REQUESTPACKET_URL=http://localhost:8080 \
		LOG_LEVEL=debug \
		NOTIFY_URL=https://s.io/pat/v1/noon \
		PARTNER_ACCT_ID=hongpay_merchant_test \
		PARTNER_ACCT_NAME=鸿支付测试商户 \
		PARTNER_ID=100001 \
		PFX_PWD=aipay123456 \
		PRIVATE_KEY_PWD=aipay654321 \
		RETURN_URL=https://as.io/#/cole/dard/ \
		DEFAULTBALANCE=69 \
		ADMINUSERS=admin datafoundry chaizs

# deploymentconfigs datafoundryserviceusage, container datafoundryserviceusage

oc env dc/datafoundryserviceusage ENV_NAME_DATAFOUNDRYPAYMENT_SERVICE_HOST=DATAFOUNDRYPAYMENT_SERVICE_HOST \
		ENV_NAME_DATAFOUNDRYPAYMENT_SERVICE_PORT=DATAFOUNDRYPAYMENT_SERVICE_PORT \
		ENV_NAME_DATAFOUNDRYPLAN_SERVICE_HOST=DATAFOUNDRYPLAN_SERVICE_HOST \
		ENV_NAME_DATAFOUNDRYPLAN_SERVICE_PORT=DATAFOUNDRYPLAN_SERVICE_PORT \
		ENV_NAME_DATAFOUNDRYRECHARGE_SERVICE_HOST=DATAFOUNDRYRECHARGE_SERVICE_HOST \
		ENV_NAME_DATAFOUNDRYRECHARGE_SERVICE_PORT=DATAFOUNDRYRECHARGE_SERVICE_PORT \
		ENV_NAME_MYSQL_ADDR=BSI_MYSQL_MYSQLPAYMENT_HOST \
		ENV_NAME_MYSQL_DATABASE=BSI_MYSQL_MYSQLPAYMENT_NAME \
		ENV_NAME_MYSQL_PASSWORD=BSI_MYSQL_MYSQLPAYMENT_PASSWORD \
		ENV_NAME_MYSQL_PORT=BSI_MYSQL_MYSQLPAYMENT_PORT \
		ENV_NAME_MYSQL_USER=BSI_MYSQL_MYSQLPAYMENT_USERNAME \
		LOG_LEVEL=debug \
		BSI_MYSQL_MYSQLPAYMENT_HOST=mysqle.seeer.dataos.io \
		BSI_MYSQL_MYSQLPAYMENT_NAME=7ffb88d6f \
		BSI_MYSQL_MYSQLPAYMENT_PASSWORD=7702057 \
		BSI_MYSQL_MYSQLPAYMENT_PORT=3306 \
		BSI_MYSQL_MYSQLPAYMENT_USERNAME=cd140 \
		DATAFOUNDRY_INFO_CN_NORTH_1=10.1.130.134:8443 chaizs password volume-datafoundry.app-dacp.dataos.io:80 \
		DATAFOUNDRY_INFO_CN_NORTH_2=10.1.130.134:8443 chaizs password volume-datafoundry.app-dacp.dataos.io:80 \
		SENDMESSAGE_SERVICE_ADDR=datafoxy-daty.app-dacp.dataos.io:80

# deploymentconfigs datafoundryservicevolume, container datafoundryservicevolume

oc env dc/datafoundryservicevolume DATAFOUNDRY_ADMIN_PASS= \
		DATAFOUNDRY_ADMIN_USER=chaizs \
		DATAFOUNDRY_HOST_ADDR=10.1.130.134:8443 \
		GLUSTER_ENDPOINTS_NAME=glusterfs-cluster \
		HEKETI_HOST_ADDR=127.0.0.1 \
		HEKETI_HOST_PORT=8080 \
		HEKETI_KEY= \
		HEKETI_USER= 

# deploymentconfigs dataintegration, container dataintegration
oc env dc/dataintegration BSI_MYSQL_RECHARGETEST_HOST=mysqle.oker.dataos.io \
		BSI_MYSQL_RECHARGETEST_NAME=be1b6d \
		BSI_MYSQL_RECHARGETEST_PASSWORD=68d17 \
		BSI_MYSQL_RECHARGETEST_PORT=3306 \
		BSI_MYSQL_RECHARGETEST_USERNAME=1946 \
		ENV_NAME_MYSQL_ADDR=BSI_MYSQL_RECHARGETEST_HOST \
		ENV_NAME_MYSQL_DATABASE=BSI_MYSQL_RECHARGETEST_NAME \
		ENV_NAME_MYSQL_PASSWORD=BSI_MYSQL_RECHARGETEST_PASSWORD \
		ENV_NAME_MYSQL_PORT=BSI_MYSQL_RECHARGETEST_PORT \
		ENV_NAME_MYSQL_USER=BSI_MYSQL_RECHARGETEST_USERNAME \
		LOG_LEVEL=debug \
		ADMINUSERS=admin datafoundry \
		DataFoundryRegionOneHost=https://10.1.130.134:8443

